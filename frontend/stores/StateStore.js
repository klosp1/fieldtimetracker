export const useStateStore = defineStore("StateStore", {
  state: () => ({
    clients: {
      dialogs: {
        add: false,
        edit: false,
        delete: false,
      },
      selected: null,
      newClientName: null
    },
    locations: {
      dialogs: {
        add: false,
        edit: false,
        delete: false,
      },
      selected: null,
      new: {
        Name: '',
        Zip: null,
        City: '',
        Address: '',
        ClientID: null,
      },
      edit: {
        Name: '',
        Zip: null,
        City: '',
        Address: ''
      },
    },
    workLogs: {
      dialogs: {
        add: false,
        edit: false,
        delete: false,
      },
      selected: null,
      newDate: new Date,
      new: {
        StartTime: null,
        EndTime: null,
        LocationID: null
      }
    },
  }),
  actions: {
    // Clients
    selectClient(client) {
      this.clients.selected = client;
    },
    checkActiveClient(client) {
      if (this.clients.selected) {
        return client.ID === this.clients.selected.ID ? true : false;
      } else {
        return false;
      }
    },
    resteNewClientName(){
      this.client.newClientName = ''
    },
    toggleAddClientDialog() {
      this.clients.dialogs.add = !this.clients.dialogs.add;
    },
    toggleEditClientDialog() {
      this.clients.dialogs.edit = !this.clients.dialogs.edit;
    },
    toggleDeleteClientDialog() {
      this.clients.dialogs.delete = !this.clients.dialogs.delete;
    },
    // Locations
    selectLocation(location) {
      this.locations.selected = location;
    },
    setNewLocationClientID(ClientID){
      this.locations.new.ClientID = ClientID
    },
    resetNewLocation(){
      this.locations.new={
        Name: '',
        Zip: null,
        City: '',
        Address: '',
        ClientID: null,
      }
    },
    resetEditLocation(){
      this.locations.edit={
        Name: '',
        Zip: null,
        City: '',
        Address: ''
      }
    },
    checkActiveLocation(location) {
      if (this.locations.selected) {
        return location.ID === this.locations.selected.ID ? true : false;
      } else {
        return false;
      }
    },
    toggleAddLocationDialog() {
      this.locations.dialogs.add = !this.locations.dialogs.add;
    },
    toggleEditLocationDialog() {
      this.locations.edit = this.locations.selected;
      this.locations.dialogs.edit = !this.locations.dialogs.edit;
    },
    toggleDeleteLocationDialog() {
      this.locations.dialogs.delete = !this.locations.dialogs.delete;
    },
    // getSelectedLocation(){
    //   this.locations.selected;
    // },
    // WorkLogs
    setNewWorkLog(LocationID, StartTime, EndTime){
      this.workLogs.new.LocationID = LocationID
      this.workLogs.new.StartTime = StartTime
      this.workLogs.new.EndTime = EndTime
    },
    selectWorkLog(workLog) {
      this.workLogs.selected = workLog;
    },
    checkActiveWorkLog(workLog) {
      if (this.workLogs.selected) {
        return workLog.ID === this.workLogs.selected.ID ? true : false;
      } else {
        return false;
      }
    },
    toggleAddWorkLogDialog() {
      this.workLogs.dialogs.add = !this.workLogs.dialogs.add;
    },
    toggleEditWorkLogDialog() {
      this.workLogs.dialogs.edit = !this.workLogs.dialogs.edit;
    },
    toggleDeleteWorkLogDialog() {
      this.workLogs.dialogs.delete = !this.workLogs.dialogs.delete;
    },
  },
  getters: {
    // Clients
    AddClientDialogIsActive() {
      return this.clients.dialogs.add;
    },
    EditClientDialogIsActive() {
      return this.clients.dialogs.edit;
    },
    DeleteClientDialogIsActive() {
      return this.clients.dialogs.delete;
    },
    getSelectedClient() {
      return this.clients.selected;
    },
    // Locations
    AddLocationDialogIsActive() {
      return this.locations.dialogs.add;
    },
    EditLocationDialogIsActive() {
      return this.locations.dialogs.edit;
    },
    DeleteLocationDialogIsActive() {
      return this.locations.dialogs.delete;
    },
    getSelectedLocation() {
      return this.locations.selected;
    },
    // WorkLogs
    AddWorkLogDialogIsActive() {
      return this.workLogs.dialogs.add;
    },
    EditWorkLogDialogIsActive() {
      return this.workLogs.dialogs.edit;
    },
    DeleteWorkLogDialogIsActive() {
      return this.workLogs.dialogs.delete;
    },
    getSelectedWorkLog() {
      return this.workLogs.selected;
    },
    getNewDate(){
      function zeroPad(d) {
        return ("0" + d).slice(-2)
      };
      return (`${this.workLogs.newDate.getUTCFullYear()}-${zeroPad(this.workLogs.newDate.getMonth() + 1)}-${zeroPad(this.workLogs.newDate.getDate())}`).toString();

    }
  },
});
