export const useClientsStore = defineStore('ClientsStore', {
    state: () => ({ 
      url: 'http://130.61.76.128/clients',
      clients: [],
      
    }),
    actions: {
      async fetchClients(){
        const { data } = await useFetch(this.url);
        if (data.value) {
          this.clients = data.value
          
        }
      },
      async deleteClient(client) {
        try {
          await fetch(`${this.url}/${client.ID}`, {
            method: "DELETE",
          });
          this.fetchClients();
        } catch (error) {
          console.error("Error deleting client:", error);
        }
      },
      async editClient(client, newName) {
        if (newName !== null) {
          try {
            await fetch(`${this.url}/${client}`, {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({ Name: newName }),
            });
            this.fetchClients();
          } catch (error) {
            console.error("Error editing client:", error);
          }
        }
      },
      
      async createClient(newName) {
        if (newName !== null) {
          try {
            await fetch(this.url, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({ Name: newName }),
            });
            this.fetchClients();
          } catch (error) {
            console.error("Error creating client:", error);
          }
        }
      }
    }
  })