export const useWorkLogsStore = defineStore('WorkLogsStore', {
    state: () => ({ 
      url: 'http://130.61.76.128/worklogs',
      workLogsByClient: [],
      workLogsByLocation: []
      
    }),
    actions: {
      async fetchWorkLogs(){
        const { data } = await useFetch(this.url);
        if (data.value) {
          this.workLogs = data.value
          
        }
      },
      async fetchWorkLogsByClient(client){
        const { data } = await useFetch(`${this.url}/clients/${client.ID}`);
        if (data.value) {
          this.workLogsByClient = data.value
          
        }
      },
      async fetchWorkLogsByLocation(locationID){
        console.log(`${this.url}/locations/${locationID}`);
        const { data } = await useFetch(`${this.url}/locations/${locationID}`);
        if (data.value) {
          this.workLogsByLocation = data.value
          
        }
      },
      async deleteWorkLog(workLog, location) {
        try {
          await fetch(`${this.url}/${workLog.ID}`, {
            method: "DELETE",
          });
          this.fetchWorkLogsByLocation(location.ID);
        } catch (error) {
          console.error("Error deleting workLog:", error);
        }
      },
      async editWorkLog(workLog, workLogID, client) {
        if (workLog !== null) {
          try {
            await fetch(`${this.url}/${workLogID}`, {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(workLog),
            });
            this.fetchWorkLogsByClient(client);
          } catch (error) {
            console.error("Error editing workLog:", error);
          }
        }
      },
      
      async createWorkLog(newWorkLog) {
        if (newWorkLog !== null) {
          try {
            await fetch(this.url, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(newWorkLog),
            });
            //console.log('Client: ' + client)
            this.fetchWorkLogsByLocation(newWorkLog.LocationID);
          } catch (error) {
            console.error("Error creating workLog:", error);
          }
        }
      }
    }
  })