export const useLocationsStore = defineStore('LocationsStore', {
    state: () => ({ 
      url: 'http://130.61.76.128/locations',
      locationsByClient: [],
      
    }),
    actions: {
      async fetchLocations(){
        const { data } = await useFetch(this.url);
        if (data.value) {
          this.locations = data.value
          
        }
      },
      async fetchLocationsByClient(client){
        const { data } = await useFetch(`${this.url}/clients/${client.ID}`);
        if (data.value) {
          this.locationsByClient = data.value
          
        }
      },
      async deleteLocation(location, client) {
        try {
          await fetch(`${this.url}/${location.ID}`, {
            method: "DELETE",
          });
          this.fetchLocationsByClient(client);
        } catch (error) {
          console.error("Error deleting location:", error);
        }
      },
      async editLocation(location, locationID, client) {
        if (location !== null) {
          try {
            await fetch(`${this.url}/${locationID}`, {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(location),
            });
            this.fetchLocationsByClient(client);
          } catch (error) {
            console.error("Error editing location:", error);
          }
        }
      },
      
      async createLocation(newLocation, client) {
        if (newLocation !== null) {
          try {
            await fetch(this.url, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(newLocation),
            });
            //console.log('Client: ' + client)
            this.fetchLocationsByClient(client.value);
          } catch (error) {
            console.error("Error creating location:", error);
          }
        }
      }
    }
  })