// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: {
    enabled: true,

    timeline: {
      enabled: true
    }
  },
  imports: {
    dirs: ['stores'],
  },
  modules: [
    'vuetify-nuxt-module',
    [
      '@pinia/nuxt',
      {
        autoImports: ['defineStore', 'storeToRefs' ],
      },
    ],
  ],
})