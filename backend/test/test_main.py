import pytest
import asyncio
import pathlib
import json
from typing import AsyncGenerator
from httpx import AsyncClient

from app.main import app

file = pathlib.Path("backend/test/data/testdata_main.json")
with open(file) as f:
    test_data_main = json.load(f)

file = pathlib.Path("backend/test/data/testdata_clients.json")
with open(file) as f:
    test_data_clients = json.load(f)

file = pathlib.Path("backend/test/data/testdata_locations.json")
with open(file) as f:
    test_data_locations = json.load(f)

file = pathlib.Path("backend/test/data/testdata_worklogs.json")
with open(file) as f:
    test_data_worklogs = json.load(f)

@pytest.fixture(scope="session")
def event_loop() -> AsyncGenerator:
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_main["test_pong"])
async def test_pong(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_clients["test_clients_get_empty_table"])
async def test_clients_get_empty_table(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_clients["test_clients_post"])
async def test_clients_post(test_case):
    endpoint = test_case["endpoint"]
    input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.post(endpoint, json=input_data)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_clients["test_clients_put"])
async def test_clients_put(test_case):
    endpoint = test_case["endpoint"]
    input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.put(endpoint, json=input_data)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_clients["test_clients_get"])
async def test_clients_get(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_clients["test_clients_delete"])
async def test_clients_delete(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.delete(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_locations["test_locations_get_empty_table"])
async def test_locations_get_empty_table(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_locations["test_locations_post"])
async def test_locations_post(test_case):
    endpoint = test_case["endpoint"]
    input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.post(endpoint, json=input_data)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_locations["test_locations_put"])
async def test_locations_put(test_case):
    endpoint = test_case["endpoint"]
    input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.put(endpoint, json=input_data)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_locations["test_locations_get"])
async def test_locations_get(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_locations["test_locations_delete"])
async def test_locations_delete(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.delete(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_locations["test_locations_get_by_id"])
async def test_locations_get_by_id(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_locations["test_locations_by_client_id"])
async def test_locations_by_client_id(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_worklogs["test_worklogs_get_empty_table"])
async def test_worklogs_get_empty_table(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_worklogs["test_worklogs_post"])
async def test_worklogs_post(test_case):
    endpoint = test_case["endpoint"]
    input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.post(endpoint, json=input_data)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_worklogs["test_worklogs_put"])
async def test_worklogs_put(test_case):
    endpoint = test_case["endpoint"]
    input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.put(endpoint, json=input_data)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_worklogs["test_worklogs_delete"])
async def test_worklogs_delete(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.delete(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_worklogs["test_worklogs_get_by_location"])
async def test_worklogs_get_by_location(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data

@pytest.mark.asyncio
@pytest.mark.parametrize("test_case", test_data_worklogs["test_worklogs_get_by_client"])
async def test_worklogs_get_by_client(test_case):
    endpoint = test_case["endpoint"]
    #input_data = test_case["input_data"]
    status_code = test_case["response"]["status_code"]
    response_data = test_case["response"]["data"]
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get(endpoint)
    assert response.status_code == status_code
    assert response.json() == response_data