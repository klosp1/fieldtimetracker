from fastapi import Depends, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from sqlmodel import select
from sqlmodel.ext.asyncio.session import AsyncSession

from app.db import get_session
from app.models import Client, ClientCreate, ClientUpdate, Location, LocationCreate, LocationUpdate, WorkLog, WorkLogCreate,  WorkLogUpdate

app = FastAPI()

# CORS middleware engedélyezése
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # "*" a kliensek bármely eredetét elfogadja; módosítsd megfelelően
    allow_credentials=True,
    allow_methods=["*"],  # Engedélyezett HTTP módszerek
    allow_headers=["*"],  # Engedélyezett HTTP fejlécek
)

@app.get("/ping")
async def pong():
    return {"ping": "pong!"}

@app.get("/clients", response_model=list[Client])
async def get_clients(session: AsyncSession = Depends(get_session)):
    result = await session.exec(select(Client))
    clients = result.all()
    return [Client(Name=client.Name, ID=client.ID) for client in clients]

@app.post("/clients")
async def add_client(client: ClientCreate, session: AsyncSession = Depends(get_session)):
    client = Client(Name=client.Name)
    session.add(client)
    await session.commit()
    await session.refresh(client)
    return client

@app.put("/clients/{client_id}")
async def update_client(client_id: int, updated_client: ClientUpdate, session: AsyncSession = Depends(get_session)):
    client = await session.get(Client, client_id)
    if client is None:
        raise HTTPException(status_code=404, detail="Client not found")
    client.Name = updated_client.Name
    await session.commit()
    await session.refresh(client)
    return client

@app.delete("/clients/{client_id}")
async def delete_client(client_id: int, session: AsyncSession = Depends(get_session)):
    client = await session.get(Client, client_id)
    if client is None:
        raise HTTPException(status_code=404, detail="Client not found")
    await session.delete(client)
    await session.commit()
    return {"message": "Client deleted successfully"}

@app.get("/locations", response_model=list[Location])
async def get_locations(session: AsyncSession = Depends(get_session)):
    result = await session.exec(select(Location))
    locations = result.all()
    return [Location(Name=location.Name, ID=location.ID, ClientID=location.ClientID, Zip=location.Zip, City=location.City, Address=location.Address) for location in locations]

@app.get("/locations/{location_id}")
async def get_location_by_id(location_id: int, session: AsyncSession = Depends(get_session)):
    location = await session.get(Location, location_id)
    if location is None:
        raise HTTPException(status_code=404, detail="Location not found")
    return Location(Name=location.Name, ID=location.ID, ClientID=location.ClientID, Zip=location.Zip, City=location.City, Address=location.Address)

@app.get("/locations/clients/{client_id}", response_model=list[Location])
async def get_locations_by_client_id(client_id: int, session: AsyncSession = Depends(get_session)):
    client = await session.get(Client, client_id)
    if client is None:
        raise HTTPException(status_code=404, detail=f"Client with ID {client_id} not found")
    # Lekérdezzük a klienshez tartozó lokációkat
    result = await session.exec(select(Location).where(Location.ClientID == client_id))
    locations = result.all()
    return [Location(Name=location.Name, ID=location.ID, ClientID=location.ClientID, Zip=location.Zip, City=location.City, Address=location.Address) for location in locations]

@app.post("/locations")
async def add_location(location: LocationCreate, session: AsyncSession = Depends(get_session)):

    existing_client = await session.exec(select(Client).where(Client.ID == location.ClientID))
    if not existing_client.first():
        raise HTTPException(status_code=404, detail=f"Client with ID {location.ClientID} not found")
    
    location = Location(Name=location.Name, ClientID=location.ClientID, Zip=location.Zip, City=location.City, Address=location.Address)
    session.add(location)
    await session.commit()
    await session.refresh(location)
    return location

@app.put("/locations/{location_id}")
async def update_location(location_id: int, updated_location: LocationUpdate, session: AsyncSession = Depends(get_session)):
    location = await session.get(Location, location_id)
    if location is None:
        raise HTTPException(status_code=404, detail="Location not found")
    location.Name = updated_location.Name
    location.Zip=updated_location.Zip
    location.City=updated_location.City
    location.Address=updated_location.Address
    await session.commit()
    await session.refresh(location)
    return location

@app.delete("/locations/{location_id}")
async def delete_location(location_id: int, session: AsyncSession = Depends(get_session)):
    location = await session.get(Location, location_id)
    if location is None:
        raise HTTPException(status_code=404, detail="Location not found")
    await session.delete(location)
    await session.commit()
    return {"message": "Location deleted successfully"}

@app.get("/worklogs", response_model=list[WorkLog])
async def get_worklogs(session: AsyncSession = Depends(get_session)):
    result = await session.exec(select(WorkLog))
    worklogs = result.all()
    return [WorkLog(ID=worklog.ID, StartTime=worklog.StartTime, EndTime=worklog.EndTime, LocationID=worklog.LocationID) for worklog in worklogs]

@app.post("/worklogs")
async def add_worklog(worklog: WorkLogCreate, session: AsyncSession = Depends(get_session)):

    existing_client = await session.exec(select(Location).where(Location.ID == worklog.LocationID))
    if not existing_client.first():
        raise HTTPException(status_code=404, detail=f"Location with ID {worklog.LocationID} not found")
    
    worklog = WorkLog(StartTime=worklog.StartTime, EndTime=worklog.EndTime, LocationID=worklog.LocationID)
    session.add(worklog)
    await session.commit()
    await session.refresh(worklog)
    return worklog

@app.put("/worklogs/{worklog_id}")
async def update_worklog(worklog_id: int, updated_worklog: WorkLogUpdate, session: AsyncSession = Depends(get_session)):
    worklog = await session.get(WorkLog, worklog_id)
    if worklog is None:
        raise HTTPException(status_code=404, detail="WorkLog not found")
    worklog.StartTime = updated_worklog.StartTime
    worklog.EndTime = updated_worklog.EndTime
    await session.commit()
    await session.refresh(worklog)
    return worklog

@app.delete("/worklogs/{worklog_id}")
async def delete_worklog(worklog_id: int, session: AsyncSession = Depends(get_session)):
    worklog = await session.get(WorkLog, worklog_id)
    if worklog is None:
        raise HTTPException(status_code=404, detail="WorkLog not found")
    await session.delete(worklog)
    await session.commit()
    return {"message": "WorkLog deleted successfully"}

@app.get("/worklogs/locations/{location_id}", response_model=list[WorkLog])
async def get_worklogs_by_location_id(location_id: int, session: AsyncSession = Depends(get_session)):
    worklog = await session.get(Location, location_id)
    if worklog is None:
        raise HTTPException(status_code=404, detail=f"Location with ID {location_id} not found")
    # Lekérdezzük a klienshez tartozó lokációkat
    result = await session.exec(select(WorkLog).where(WorkLog.LocationID == location_id))
    worklogs = result.all()
    return [WorkLog(ID=worklog.ID, StartTime=worklog.StartTime, EndTime=worklog.EndTime, LocationID=worklog.LocationID) for worklog in worklogs]


@app.get("/worklogs/client/{client_id}", response_model=list[WorkLog])
async def get_worklogs_by_client_id(client_id: int, session: AsyncSession = Depends(get_session)):
    client = await session.get(Client, client_id)
    if client is None:
        raise HTTPException(status_code=404, detail=f"Client with ID {client_id} not found")
    result = await session.exec(
        select(WorkLog).join(Location).join(Client).where(Client.ID == client_id)
    )
    worklogs = result.all()
    return worklogs
