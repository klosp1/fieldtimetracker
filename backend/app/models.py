from sqlmodel import SQLModel, Field
from typing import Optional
from datetime import datetime



# Clients
class ClientBase(SQLModel):
    Name: str

class Client(ClientBase, table=True):
    __tablename__ = "clients"

    ID: int = Field(default=None, nullable=False, primary_key=True)


class ClientCreate(ClientBase):
    pass

class ClientUpdate(ClientBase):
    pass

# Location
class LocationBase(SQLModel):
    Name: str
    Zip: Optional[int]
    City: Optional[str]
    Address: Optional[str]
    

class Location(LocationBase, table=True):
    __tablename__ = "locations"

    ID: int = Field(default=None, nullable=False, primary_key=True)
    ClientID: int = Field(foreign_key="clients.ID")

class LocationCreate(LocationBase):
    ClientID: int

class LocationUpdate(LocationBase):
    pass

# WorkLog
class WorkLogBase(SQLModel):
    StartTime: datetime
    EndTime: datetime
    

class WorkLog(WorkLogBase, table=True):
    __tablename__ = "worklogs"

    ID: int = Field(default=None, nullable=False, primary_key=True)
    LocationID: int = Field(foreign_key="locations.ID")

class WorkLogCreate(WorkLogBase):
    LocationID: int

class WorkLogUpdate(WorkLogBase):
    pass